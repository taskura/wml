from django.contrib.auth import (
    authenticate,
    get_user_model,
    login, logout,
    update_session_auth_hash
)
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib import messages
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _
from .forms import UserLoginForm, UserRegisterForm

from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from django.views.generic.detail import DetailView

from workorder.models import TimeCard, WorkOrder

class UserProfile(DetailView):
    model = User
    template_name = 'profile.html'
    context_object_name = 'xuser' 

    def get_context_data(self,**kwargs):
        data = super(UserProfile,self).get_context_data(**kwargs)
        total_hr = 0
        try:
            time_cards = TimeCard.objects.filter(employee_name=self.request.user.employee,approved=True)
            for tc in time_cards:
                total_hr = total_hr + int(tc.duration)
        except Exception as e:
            pass
    
        data["total_hr"] = total_hr
        return data

def login_view(request):
    if request.user.is_authenticated:
        return redirect("/")
    next = request.GET.get('next')
    title = _("Login")
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        if next:
            return redirect(next)
        return redirect("/")
    context = {
        "form": form,
        "title": title
    }
    return render(request, "registration/login.html", context)


def register_view(request):
    title = _("Register")
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        new_user = authenticate(username=user.username, password=password)
        login(request, new_user)
        return redirect("/")

    context = {
        "form": form,
        "title": title
    }
    return render(request, "registration/registration.html", context)


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, _('Your password was successfully updated!'))
            return redirect('/')
        else:
            messages.error(request, _('Please correct the error below.'))
    else:
        form = PasswordChangeForm(request.user)
    context = {
        "form": form,
    }
    return render(request, "registration/change_password.html", context)




def logout_view(request):
    logout(request)
    return redirect("/")