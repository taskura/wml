from django.forms import ModelForm, inlineformset_factory, TextInput
from .models import (
	WorkOrder,
	Employee,
	Customer,
	Purchase,
	Task,
	TimeCard,
	Documentation,
	Item,
	Invoice
	)





class WorkOrderForm(ModelForm):
	class Meta:
		model = WorkOrder
		exclude = ['created_by','modified_by','all_tasks_complete','is_template','initial_deu_date']
		widgets={
			'customer_contact_date': TextInput(attrs={'type': 'date'}),
			'expiration_date': TextInput(attrs={'type': 'date'}),
			'promised_delivery_date': TextInput(attrs={'type': 'date'}),
			'quote_sent_date': TextInput(attrs={'type': 'date'})
		}

class EmployeeForm(ModelForm):
	class Meta:
		model = Employee
		exclude = ['terminate_date','terminated']
		widgets={
			'start_date': TextInput(attrs={'type': 'date'})
		}

class EmployeeUpdateForm(ModelForm):
	class Meta:
		model = Employee
		fields = '__all__'
		widgets={
			'start_date': TextInput(attrs={'type': 'date'}),
			'terminate_date': TextInput(attrs={'type': 'date'})
		}

class CustomerForm(ModelForm):
	class Meta:
		model = Customer
		fields = '__all__'



class TaskForm(ModelForm):
	class Meta:
		model = Task
		exclude = ['task_duration']
		widgets={
			'start_date': TextInput(attrs={'type': 'date'}),
			'end_date': TextInput(attrs={'type': 'date'}),
			'task_due_date': TextInput(attrs={'type': 'date'}),
		}


from django import forms

class TimeCardForm(ModelForm):
	start_time = forms.DateTimeField(input_formats=["%m-%d-%Y %H:%M"])
	end_time = forms.DateTimeField(input_formats=["%m-%d-%Y %H:%M"])
	class Meta:
		model = TimeCard
		fields = ["start_time","end_time","employee_name","work_order"]


class DocumentationForm(ModelForm):
	class Meta:
		model = Documentation
		fields = '__all__'



class PurchaseForm(ModelForm):
	class Meta:
		model = Purchase
		exclude = ['po_cost','approved']
		widgets={
			'ordered_date': TextInput(attrs={'type': 'date'}),
			'required_date': TextInput(attrs={'type': 'date'}),
			'promised_delivery_date': TextInput(attrs={'type': 'date'})
		}


class InvoiceForm(ModelForm):
	class Meta:
		model = Invoice
		exclude = [
				'work_order',
				'invoice_status',
				'email_to',
				'description',
				'payment_details',
				'invoice_customer',
				'external_account_code',
				'is_created',
			]
		widgets={
			'invoice_date': TextInput(attrs={'type': 'date'}),
			'expected_payment_date': TextInput(attrs={'type': 'date'})
		}	


class ItemInlineForm(ModelForm):
	class Meta:
		model = Item
		exclude = ['line_cost','gst','mp','ma','is_markuped']



PurchaseItemInlineForm = inlineformset_factory(Purchase, Item,form=ItemInlineForm, can_delete=True, extra=0,min_num=1)