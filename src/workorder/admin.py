from django.contrib import admin

# Register your models here.
from .models import (
	Customer,
	WorkOrder,
	Documentation,
	Employee,
	Task,
	TimeCard,
	Purchase,
	Invoice,
	Item,
	Preference

	)

admin.site.register(Customer)
admin.site.register(WorkOrder)
admin.site.register(Documentation)
admin.site.register(Employee)
admin.site.register(Task)
admin.site.register(TimeCard)
admin.site.register(Purchase)
admin.site.register(Invoice)
admin.site.register(Item)
admin.site.register(Preference)

