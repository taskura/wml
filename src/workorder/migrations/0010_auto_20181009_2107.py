# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-10-09 21:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workorder', '0009_auto_20181009_1628'),
    ]

    operations = [
        migrations.AlterField(
            model_name='timecard',
            name='end_time',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='timecard',
            name='start_time',
            field=models.DateTimeField(),
        ),
    ]
