var wmlloadForm = function () {
    var btn = $(this);
    
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#editcontainer").html("");
        
      },
      success: function (data) {
        $("#editcontainer").html(data.html_form);
        $("#updateModal").modal('show')

      }
    });
  };

var wmlloadTaskForm = function () {
    var btn = $(this);
    
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#editcontainertask").html("");
        
      },
      success: function (data) {
        $("#editcontainertask").html(data.html_form);
        $("#updateModalTask").modal('show')

      }
    });
  };


$("[id='doc_edit']").click(wmlloadForm);
$("[id='timecard_edit']").click(wmlloadForm);
$("[id='task_edit']").click(wmlloadTaskForm);
$("[id='customer_edit']").click(wmlloadForm);
$("[id='employee_edit']").click(wmlloadForm);
$("[id='workorder_edit']").click(wmlloadForm);
$("[id='purchase_edit']").click(wmlloadForm);
$("[id='purchase_create']").click(wmlloadForm);
$("[id='invoice_edit']").click(wmlloadForm);


function get_active(path){
if (path.match(/customer/g)) {
    $('#id_customer').addClass('xactive');
  } 
  else if (path.match(/employee/g)) {
    $('#id_employee').addClass('xactive');
  }
  else if (path.match(/purchase/g)) {
    $('#id_purchase').addClass('xactive');
  }
  else if (path.match(/task/g)) {
    $('#id_task').addClass('xactive');
  }
  else if (path.match(/timecard/g)) {
    $('#id_timecard').addClass('xactive');
  }
  else if (path.match(/documentation/g)) {
    $('#id_documentation').addClass('xactive');
  }
  else if (path.match(/invoice/g)) {
    $('#id_invoice').addClass('xactive');
  }
  else{
    $('#id_workorder').addClass('xactive');
  }
}


$(document).ready(function() {
  get_active(window.location.pathname.split( '/' )[1]);

 $("[id='form_datetime']").datetimepicker({format: 'dd-mm-yyyy hh:ii'});

});