var json = {
    pages: [
    {
      questions: [
        {
          type: "paneldynamic",
          name: "q1",
          title: "who was the priminister in 1971 ?",
          templateElements: [
          {
              type: "rating",
              name: "rating",
              titleLocation: "hidden",
              rateMax: 10
            },
            {
              type: "comment",
              name: "additional message",
              title: "additional answer",
              titleLocation: "hidden",
              rows: 1,
              placeHolder: "additional answer"
            }
          ],
          allowRemovePanel: false,
          panelCount: 1,
          minPanelCount: 1,
          maxPanelCount: 1
        }
      ]
    },
    {
      questions: [
        {
          type: "paneldynamic",
          name: "q1",
          title: "who was the manager in 1971 ?",
          templateElements: [
          {
              type: "rating",
              name: "rating",
              titleLocation: "hidden",
              rateMax: 10
            },
            {
              type: "comment",
              name: "additional message",
              title: "additional answer",
              titleLocation: "hidden",
              rows: 1,
              placeHolder: "additional answer"
            }
          ],
          allowRemovePanel: false,
          panelCount: 1,
          minPanelCount: 1,
          maxPanelCount: 1
        }
      ]
    },
    {
        questions: [
        {
          type: "paneldynamic",
          name: "q1",
          title: "is bangladesh gov help popel ?",
          templateElements: [
            {
              type: "rating",
              name: "rating",
              titleLocation: "hidden",
              rateMax: 10
            },
            {
              type: "comment",
              name: "additional message",
              title: "additional answer",
              titleLocation: "hidden",
              rows: 1,
              placeHolder: "additional answer"
            }
          ],
          allowRemovePanel: false,
          panelCount: 1,
          minPanelCount: 1,
          maxPanelCount: 1
        }
      ]
    }
  ]
};